import { expect } from 'chai';
import App from 'app/app';
import { Provider } from 'preact-redux';
import { getFakeStore } from 'spec/support/store';
import moxios from 'moxios';

describe('Authentication spec', () => {
  let scratch;

  const mount = (jsx, store) => {
    render(<Provider store={store}>{jsx}</Provider>, scratch);
  };

  beforeEach(() => {
    scratch = document.createElement('div', { id: 'app' });
    moxios.install();
    moxios.stubRequest('http://localhost:4000/api/graphql', {
      status: 200,
      responseText: JSON.stringify({
        data: {
          me: {
            id: 1, name: 'Nick'
          }
        }
      })
    });
  });

  afterEach(() => {
    document.innerHtml= '';
    moxios.uninstall();
  });

  it('renders the authentication page when not signed in', () => {
    mount(<App />, getFakeStore({ authentication: { token: null, status: 'logged_out' } }));

    expect(scratch.querySelector('h2').textContent).to.eq('Sign in');
  });

  it('renders the application page when signed in', () => {
    mount(<App />, getFakeStore({ authentication: { token: '123', status: 'success' } }));

    expect(scratch.querySelector('h2').textContent).to.eq('Your collection');
  });
});
