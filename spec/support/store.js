import configureStore from 'redux-mock-store';
import reducers from 'app/reducers/index';
import sinon from 'sinon';

export const getFakeStore = (state = {}) => {
  return {
    getState: function() { return state; },
    subscribe: function() { return; }
  };
};
