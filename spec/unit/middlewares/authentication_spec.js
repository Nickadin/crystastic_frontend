import { expect } from 'chai';
import { createMockStore } from 'redux-logic-test';
import target from 'app/middlewares/authentication';

import { signIn } from 'app/actions/authentication';
import reducer from 'app/reducers/index';

import moxios from 'moxios';

describe('Authentication Middleware Spec', () => {
  let store;

  beforeEach(() => {
    moxios.install();
  });

  afterEach(() => {
    moxios.uninstall();
  });

  describe('when authentication succeeds', () => {
    beforeEach(() => {
      moxios.stubRequest('http://localhost:4000/api/graphql', {
        status: 200,
        responseText: JSON.stringify({
          data: {
            login: {
              id: 1,
              token: '123'
            }
          }
        })
      });
    });

    it('sends a pending event', () => {
      store = createMockStore({
        reducer,
        logic: [target]
      });
      store.dispatch(signIn({ username: 'Foo', password: 'bar' }));

      return store.whenComplete(() => {
        const action = store.actions.find(
          ({ type, status }) => type === 'AUTHENTICATION' && status === 'pending'
        );
        return expect(action).to.exist;
      });
    });

    it('stores the token', () => {
      store = createMockStore({
        reducer,
        logic: [target]
      });
      store.dispatch(signIn({ username: 'Foo', password: 'bar' }));

      return store.whenComplete(() => {
        const { authentication } = store.getState();
        return expect(authentication.token).to.eq('123');
      });
    });
  });

  describe('when authentication fails', () => {
    beforeEach(() => {
      moxios.stubRequest('http://localhost:4000/api/graphql', {
        status: 200,
        responseText: JSON.stringify({
          data: {},
          errors: [
            {
              property: 'password',
              rule: 'invalid'
            }
          ]
        })
      });
    });

    it('registers the error', () => {
      store = createMockStore({
        reducer,
        logic: [target]
      });
      store.dispatch(signIn({ username: 'Foo', password: 'bar' }));

      return store.whenComplete(() => {
        const { authentication } = store.getState();
        return expect(authentication.status).to.eq('failure');
      });
    });
  });
});
