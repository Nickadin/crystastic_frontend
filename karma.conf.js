module.exports = function karmaConfig(config) {
  config.set({
    frameworks: ['mocha'],

    files: [
      { pattern: 'spec/**/*_spec.js', watch: true },
      { pattern: 'spec/**/**/*_spec.js', watch: true },
      { pattern: 'spec/*_spec.js', watch: true }
    ],

    preprocessors: {
      'spec/**/*.js': ['webpack'],
      'spec/**/**/*.js': ['webpack']
    },

    client: {
      captureConsole: true,
      mocha: {
        reporter: 'html' // view on http://localhost:9876/debug.html
      }
    },

    reporters: ['mocha'],
    browsers: ["ChromeHeadlessCI"],
    customLaunchers: {
      ChromeHeadlessCI: {
        base: "ChromeHeadless",
        flags: [
          "--no-sandbox" // required when running inside a Docker container
        ]
      }
    },
    phantomjsLauncher: {
      // Have phantomjs exit if a ResourceError is encountered (useful if karma exits without killing phantom)
      exitOnResourceError: true
    },
    concurrency: 1,
    webpack: require('./webpack.config.js'),
    webpackMiddleware: {
      stats: 'errors-only'
    }
  });
};