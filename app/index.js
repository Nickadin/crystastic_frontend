import App from './app';

import { Provider } from 'preact-redux';

import 'semantic-ui-css/semantic.min.css';
import { Container } from 'semantic-ui-react';

import { getStore } from 'app/store';

render(
  <Provider store={getStore()}>
    <Container>
      <App />
    </Container>
  </Provider>,
  document.getElementById('app')
);
