import Authenticated from 'app/sections/authentication/containers/authenticated';

export default () =>
  <div>
    <Authenticated>
      <h2>Your collection</h2>
    </Authenticated>
  </div>;