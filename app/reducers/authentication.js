import { AUTHENTICATION } from 'app/actions/authentication';

const initialState = { token: null, status: 'logged_out' };

export default (state = initialState, action) => {
  if (action.type === AUTHENTICATION && action.status === 'success') {
    return { ...state, token: action.payload.token, status: 'success' };
  }

  if (action.type === AUTHENTICATION) {
    return { ...state, status: action.status };
  }
  return state;
};
