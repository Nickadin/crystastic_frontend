import { connect } from 'preact-redux';
import { Form, Button } from 'semantic-ui-react';
import { withState, compose } from 'recompose';

import { signIn } from 'app/actions/authentication';
import './sign_in_form.css';

const mapDispatchToProps = { signIn };

const SigninForm = props => {
  const { username, password, setUsername, setPassword, signIn, loginStatus } = props;

  let errors = null;
  if (loginStatus === 'failure') {
    errors = <div className="error">Authentication failed</div>;
  }

  return <div>
    <h2>Sign in</h2>
    {errors}
    <Form onSubmit={() => signIn({ username, password })}>
      <Form.Field>
        <label>Username</label>
        <input
          type="text"
          id="username"
          value={username}
          onChange={e => setUsername(e.target.value)}
        />
      </Form.Field>

      <Form.Field>
        <label>Password</label>
        <input
          type="password"
          id="password"
          value={password}
          onChange={e => setPassword(e.target.value)}
        />
      </Form.Field>

      <Button type="submit">Sign in</Button>
    </Form>
  </div>;
};

export default compose(
  connect(null, mapDispatchToProps),
  withState('username', 'setUsername', ''),
  withState('password', 'setPassword', '')
)(SigninForm);
