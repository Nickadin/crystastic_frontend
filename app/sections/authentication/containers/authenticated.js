import { connect } from 'preact-redux';
import getSignInStatus from 'app/selectors/sign_in_status';
import { compose, branch, renderComponent } from 'recompose';

import SignInForm from './sign_in_form';

const mapStateToProps = state => (
  {
    loginStatus: getSignInStatus(state)
  }
);

const Authenticated = ({ children }) => <div>{children}</div>;

export default compose(
  connect(mapStateToProps),
  branch(
    ({ loginStatus }) => loginStatus !== 'success',
    renderComponent(
      connect(mapStateToProps)(SignInForm)
    )
  )
)(Authenticated);
