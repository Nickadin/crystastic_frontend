export const SIGN_IN = 'SIGN_IN';
export const AUTHENTICATION = 'AUTHENTICATION';

export const signIn = ({ username, password }) => (
  {
    type: SIGN_IN,
    payload: { username, password }
  }
);

export const authenticationPending = payload => ({
  payload,
  type: AUTHENTICATION,
  status: 'pending'
});

export const authenticationSuccess = payload => ({
  payload,
  type: AUTHENTICATION,
  status: 'success'
});

export const authenticationFailure = error => ({
  error,
  type: AUTHENTICATION,
  status: 'failure'
});
