import { createStore, compose, applyMiddleware } from 'redux';
import reducers from './reducers/index';
import middlewares from 'app/middlewares/index';

let composeEnhancers =
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
    }) : compose;

if (process.env.NODE_ENV === 'production') { //eslint-disable-line
  composeEnhancers = compose;
}

const enhancer = composeEnhancers(applyMiddleware(middlewares));

const getTokenAndState = () => {
  const token = localStorage.getItem('token');
  return {
    token,
    status: token ? 'success' : 'logged_out'
  };
};

const initialState = {
  authentication: getTokenAndState()
};

const store = createStore(reducers, initialState, enhancer);

export const getStore = () => store;
