import { createLogic } from 'redux-logic';
import { post } from 'axios';

import {
  SIGN_IN,
  authenticationPending,
  authenticationSuccess,
  authenticationFailure
} from 'app/actions/authentication';

export default createLogic({
  type: SIGN_IN,
  latest: true,
  processOptions: {
    dispatchReturn: false
  },
  process({ action }, dispatch, done) {
    const { username, password } = action.payload;
    dispatch(authenticationPending({ username }));

    const query = `mutation {
      login(username: "${username}", password: "${password}") {
        id, token
      }
    }`;

    return post(`${process.env.API}/graphql`, { query })
      .then(({ data }) => {
        // MIDDLEWARE AXIOS
        if (data.errors) {
          dispatch(authenticationFailure(data.errors));
          return;
        }

        dispatch(authenticationSuccess(data.data.login));
        return;
      }).catch(err => {
        dispatch(authenticationFailure(err));
        return;
      }).then(done);
  }
});
