import { createLogicMiddleware } from 'redux-logic';

import authenticationMiddleware from './authentication';
import sessionMiddleware from './session';

export default createLogicMiddleware([
  authenticationMiddleware,
  sessionMiddleware
]);
