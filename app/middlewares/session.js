import { createLogic } from 'redux-logic';

import {
  AUTHENTICATION
} from 'app/actions/authentication';

export default createLogic({
  type: AUTHENTICATION,
  latest: true,
  processOptions: {
    dispatchReturn: false
  },
  validate({ action }, allow, reject) {
    if (action.status === 'success') {
      return allow(action);
    }
    return reject(action);
  },
  process({ action }, _dispatch, done) {
    localStorage.setItem('token', action.payload.token);
    done();
  }
});
