import { createSelector } from 'reselect';

const getAuthStatus = state => state.authentication.status;

export default createSelector(
  [getAuthStatus],
  status => status
);
