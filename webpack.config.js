/*eslint-disable*/
var path = require('path');
var webpack = require('webpack');
var CompressionPlugin = require("compression-webpack-plugin");
var Dotenv = require('dotenv-webpack');

var APP_ENV       = process.env.APP_ENV || process.env.NODE_ENV || 'development';
var IS_PROD_BUILD = ! ["development", "test"].includes(APP_ENV);


module.exports = {
  entry: './app/index.js',
  output: {
    path: __dirname + '/dist',
    filename: 'app.js',
    publicPath: '/dist/'
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    modules: [__dirname, "node_modules"],
    alias: {
      'app': path.resolve(__dirname, 'app'),
      'spec': path.resolve(__dirname, 'spec'),
      'react': 'preact-compat',
      'react-dom': 'preact-compat'
    }
  },
  plugins: [
    new webpack.ProvidePlugin({
      h: ['preact', 'h'],
      render: ['preact', 'render']
    }),
    new Dotenv({
      path: './.env.' + APP_ENV.toLowerCase()
    })
  ],
  module: {
    loaders: [
      {
        test: /.jsx?$/,
        loader: 'babel-loader',
        query: {
          presets: ['env']
        }
      },
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ]
      },
      {
        test: /\.(png|woff|woff2|eot|ttf|svg)$/, loader: 'url-loader?limit=100000'
      }
    ]
  },
  externals: {
    'jsdom': 'window'
  }
};

if (IS_PROD_BUILD) {
  module.exports.plugins.push(new webpack.optimize.UglifyJsPlugin({minimize: true}));
  module.exports.plugins.push(new CompressionPlugin({
    asset: "[file].gz",
    algorithm: "gzip",
    test: /\.(js|css|html)$/
  }));
  module.exports.plugins.push(new webpack.DefinePlugin({
    "process.env": {
      NODE_ENV: JSON.stringify("production")
    }
  }));
} else {
  var exclude = /node_modules/
  module.exports.module.loaders[0].exclude = exclude
  module.exports.devtool = 'inline-sourcemap'
}